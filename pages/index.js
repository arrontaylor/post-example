import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { Button, Field, Input, Column} from "rbx";
import {useState} from 'react';
import "rbx/index.css";
import axios from 'axios'

export default function Submit() {

  const [user, setUser] = useState({name:'', email:'', password:''});

  const HandleSubmit = (e) => {
    e.preventDefault();
    axios({
      method: 'post',
      url: 'https://df37f862cc8c4cb705733e49001ad4ab.m.pipedream.net',
      data: {
        firstName: user.name,
        email: user.email, 
        password: user.password
      }
    }).then(function (response) {
    console.log(response);
  });
  }
  const handleField = (e) => {
    const { name, value } = e.target;
    setUser(prevState => ({ ...prevState,[name]: value}));
  }
  return (
    <div>
    <Column size='one-third'> 
      <form onSubmit={HandleSubmit}>
        <Field>
          <Input type="text" name='name' onChange={handleField} value={user.name} placeholder="name..." />
        </Field>
        <Field>
          <Input type="text" name='email' onChange={handleField} value={user.email} placeholder="email..." />
        </Field>
         <Field>
          <Input type="password" name='password' onChange={handleField} value={user.password} placeholder="password..." />
        </Field>
        <Field>
          <Button type='submit'> Submit </Button>
        </Field>
      </form>
    </Column>
    </div>
  )
}
